import React, { Component } from 'react';
import { Text, ScrollView, View } from 'react-native';
import { createDrawerNavigator, createAppContainer } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome5'

import Feed from './src/routes/Feed';
import Perfil from './src/routes/Perfil';

const AppNavigator = createDrawerNavigator({
  Feed: {
    screen:Feed,
    navigationOptions:{
      drawerIcon: () => (
        <Icon name="surprise" size={20} />
      )
    }
  },
  Perfil: {
    screen:Perfil,
    navigationOptions:{
      drawerIcon: () => (
        <Icon name="cogs" size={20} />
      )
    }
  },
}, {
   //contentComponent: props => 
  //<View><Text>Olá</Text></View>,
  drawerPosition:'left',
  drawerBackgroundColor:'#0066CC',
  contentOptions:{
    activeTintColor:'#FFF',
    inactiveTintColor:'#000'
  }
});

export default createAppContainer(AppNavigator);