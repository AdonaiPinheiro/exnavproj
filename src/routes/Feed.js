import React, { Component } from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';

export default class Tela1 extends Component {

	static navigationOptions = ({navigation}) => ({
		title:"Feed",
	});

	render() {
		return (
			<View style={styles.container}>
				<Text>Feed</Text>

				<Button title="Ir para perfil" onPress={() => this.props.navigation.navigate("Perfil")} />
			</View>
		);
	}

}

const styles = StyleSheet.create({
	container:{
		flex:1,
		justifyContent:'center',
		alignItems:'center',
	}
});




