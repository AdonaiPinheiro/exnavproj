import React, { Component } from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';

export default class Tela2 extends Component {

	static navigationOptions = ({navigation}) => ({
		title:"Perfil",
	});

	constructor(props) {
		super(props);
		this.state = {};

		this.abrirTela = this.abrirTela.bind(this);
	}

	abrirTela() {
		this.props.navigation.navigate("Feed");
	}

	render() {
		return (
			<View style={styles.container}>
				<Text>Perfil</Text>
			</View>
		);
	}

}

const styles = StyleSheet.create({
	container:{
		flex:1,
		justifyContent:'center',
		alignItems:'center',
		marginTop:20
	}
});




